import mongoose, { Schema, Document } from 'mongoose';
import differenceInMinutes from 'date-fns/difference_in_minutes';

export interface IUser extends Document {
  id: string;
  last_seen?: Date;
}

const UserSchema = new Schema(
  {
    id: {
      type: String,
      require: 'Id is required',
      unique: true,
    },
    last_seen: {
      type: Date,
      default: new Date(),
    },
  },
  {
    timestamps: true,
  },
);

UserSchema.virtual('isOnline').get(function(this: any) {
  return differenceInMinutes(new Date().toISOString(), this.last_seen) < 5;
});

UserSchema.set('toJSON', {
  virtuals: true,
});

// UserSchema.pre('save', async function(next) {
//   const user: any = this;
//
//   if (!user.isModified('password')) {
//     return next();
//   }
//
//   user.password = await generatePasswordHash(user.password);
//   user.confirm_hash = await generatePasswordHash(new Date().toString());
// });

const UserModel = mongoose.model<IUser>('User', UserSchema);

export default UserModel;
