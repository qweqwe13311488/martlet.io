import jwt from 'jsonwebtoken';

export default (token: string) =>
  new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET || 'eQ<P{aTc@cEJ*a!L3<h]F+%x6{Ju3rzD', (err, decodedData) => {
      if (err || !decodedData) {
        return reject(err);
      }

      resolve(decodedData);
    });
  });
