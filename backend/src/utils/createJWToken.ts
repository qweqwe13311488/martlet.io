import jwt from 'jsonwebtoken';
import { reduce } from 'lodash';
import { IUser } from '../models/User';

interface ILoginData {
  id: string;
}

export default (user: ILoginData) => {
  let token = jwt.sign(
    {
      data: reduce(
        user,
        (result: any, value: string, key: string) => {
          if (key !== 'password') {
            result[key] = value;
          }
          return result;
        },
        {},
      ),
    },
    process.env.JWT_SECRET || 'eQ<P{aTc@cEJ*a!L3<h]F+%x6{Ju3rzD',
    {
      expiresIn: "7d",
      algorithm: 'HS256',
    },
  );

  return token;
};
