import axios from "axios";

axios.defaults.baseURL = "http://188.225.82.241:3003";
axios.defaults.headers.common["token"] = window.localStorage.token;

window.axios = axios;

export default axios;
