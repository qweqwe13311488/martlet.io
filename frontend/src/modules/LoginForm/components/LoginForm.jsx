import React from "react";
import { Button, Block } from "components";
import { validateField } from "utils/helpers";

const LoginForm =  ({userId,handleSubmit,isSubmit}) => {
    let id = '';
    id = userId;
  return (
    <div className="auth__positioner">
      <div className="auth__top">
        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"/>
        <h2>Добро пожаловать</h2>
        <p>Пожалуйста, передайте собеседнику ваш идентификатор</p>
      </div>
      <Block>
        <div className="login-form">
          <p className="login-form--id">{
              id.toString() === "[object Object]" ? "" : id.toString()
          }</p>
          <Button
              disabled={isSubmit}
              onClick={e => handleSubmit(e,userId)}
              type="primary"
              size="large"
          >
            Начать общение
          </Button>
        </div>
      </Block>
    </div>
  );
};

export default LoginForm;