import React, { useState, useEffect } from 'react';
import LoginForm from '../components/LoginForm';

import { userActions } from 'redux/actions';

import store from 'redux/store';


function LoginFormContainer(props) {
  useEffect(() => {
    store.dispatch(userActions.fetchUserData());
  },[]);

  let [userId, setUserId] = useState({userId:""});
  store.subscribe(() => {
    setUserId(store.getState().user.data.id.toString());
  });

  return <LoginForm userId={userId} handleSubmit={handleSubmit} isSubmit={isSubmit}/>
}

let isSubmit = false;
const handleSubmit =  (e, id) => {
    e.preventDefault();
    isSubmit = true;
    let values = {
        id : id
    };

    store
    .dispatch(userActions.fetchUserLogin(values))
    .then(({ status }) => {
        if (status === 'success') {
            window.location.href = "/";
            isSubmit = false;
    }

})
.catch(() => {
    isSubmit = false;
});
};


export default LoginFormContainer;