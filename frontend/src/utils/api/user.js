import { axios } from "core";

export default {
  signIn: postData => axios.post("/user/signin", postData),
  verifyHash: hash => axios.get("/user/verify?hash=" + hash),
  getMe: () => axios.get("/user/me"),
  getMessages: () => axios.get("/user/messages"),
  findUsers: query => axios.get("/user/find?query=" + query)
};
